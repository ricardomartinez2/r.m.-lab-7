Ricardo Martinez

Lab 7 - Number Pad & Passcode

EEE 174

Sean Kennedy

Thursday 6:30-9:10 PM




**Introduction:**
The objective of this lab is to use the raspberry pi GPIO pins and a number pad to create a digital lock with a passcode. In this lab I write a program in python that takes in the 16 different inputs form a 4x4 number pad using matrices and executes a certain command if the passcode is entered correctly or incorrectly. Creating a simple variable named passcode I then am able to compare it with the user input and either unlock the digital lock or keep it locked. This digital lock will then be implemented in our final project to control a physical lock.




**Description of Problem:**
Create a program that acts as a digital lock with inputs taken from a 4x4 number pad and unlocks with a passcode.




**Work Performed / Solution:**
For this lab I first did some research on the 4x4 numberpad to understand how it works and how the inputs can be defined by a matrix variable in python. Using the reference linked;
	
	
	http://www.circuitbasics.com/how-to-set-up-a-keypad-on-an-arduino/
	
I was able to understand how the inputs from the number pad are interpreted by the raspberry pi. It was not as straightforward as I first thought it would be but after reading the article I had a decent understanding of how the raspberry pi receives the inputs from the number pad. The first four wires are rows 1-4 and the following wires are the columns 5-8.
The next part was the hardest but I found a good explanation of the passcode code in python by following the link below;


	https://www.raspberrypi.org/forums/viewtopic.php?t=208792
	
Using the code found in this link and tweaking it slightly for our project compiled properly and was able to run properly using the number pad as the inputs and a simple variable to define the passcode.



**Conclusion:**
For this lab I programmed a digital lock that unlocks with inputs from a number pad and a passcode. I first did research on the 4x4 numberpad. I learned the first four wires from left to right are the row wires and the next four are the columns. After learning this I understood how it�s supposed to be wired up. I also learned in order for the raspberry pi to interpret the inputs, the number pad had to be defined as a matrix. Next the passcode to trigger the digital lock was made by creating a passcode variable and comparing it to the input received from the number pad. This was one of the most important parts for our project as this is what unlocks our �mini safe�.
 